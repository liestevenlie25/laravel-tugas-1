<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('Register');
    }
    public function welcome(Request $request)
    {
        // dd($request->all());
        $namaDepan = $request['depan'];
        $namaBelakang = $request['belakang'];

        return view('Welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
