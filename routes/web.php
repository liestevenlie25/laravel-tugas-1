<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/pendaftaran', [AuthController::class, 'daftar']);
Route::post('/kirim', [AuthController::class, 'welcome']);

Route::get('/data-table', function(){
    return view('data-table');
});
//CRUD Cast

//Create Data
// mengarah form tambah data

Route::get('/cast/create', [CastController::class, 'create']);
//menyimpan data ke table kategori DB
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//mengambil semua data di database
Route::get('cast',[CastController::class, 'index']);
//detail cast ambil berdasar id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
//Update Data
//mengarah ke form edit data kategori
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//update data berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//delete data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);