@extends('layouts.master')

@section('title')
Halaman Table Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Create</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">First</th>
        <th scope="col">Last</th>
        <th scope="col">Handle</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($data as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    
                    <form action="/cast/{{$item->id}}" method="POST">
                      <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                      <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                      @method('delete')
                      @csrf
                      <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </td>
            </tr>
        @empty
            <h1>Data Cast Film</h1>
        @endforelse
    </tbody>
  </table>

@endsection