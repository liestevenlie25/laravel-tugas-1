@extends('layouts.master')

@section('title')
Buat Account Baru!
@endsection
        
@section('content')
    <form action="/kirim" method="POST">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="depan"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="belakang"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        <label>Nationality:</label><br><br>
        <select>
            <option value="Indonesian">Indonesian</option><br>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="skill">Bahasa Indonesia<br>
        <input type="checkbox" name="skill">English<br>
        <input type="checkbox" name="skill">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br><br>
        
            <input type="submit" value="Sign Up">
    </form>
@endsection